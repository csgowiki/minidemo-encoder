package main

import (
	iparser "github.com/hx-w/minidemo-encoder/internal/parser"
)

func main() {
	iparser.Start()
}
